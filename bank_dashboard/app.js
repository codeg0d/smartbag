const users = document.getElementById('users')
const form = document.querySelector('.section-one')

const request = document.getElementById('bag')
const requestform = document.querySelector('.request')

const screening = document.getElementById('requestScreening')
const formRequest = document.querySelector('.request-screening')


request.addEventListener('click', toggleRequest);
users.addEventListener('click', toggleForm);
screening.addEventListener('click', screeningRequest)

function toggleRequest() {
  if(requestform.style.display === 'none') {
    requestform.style.display = 'block'
  } else requestform.style.display = 'none'
}
function toggleForm() {
  if(!users.classList.contains('show')) {
    form.classList.toggle('show')
  }
}

function screeningRequest() {
  if(formRequest.style.display === 'none'){
    formRequest.style.display = 'block'
  } else formRequest.style.display = 'none'
}

