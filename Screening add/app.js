const users = document.getElementById('users');
const form = document.querySelector('.section-one');

users.addEventListener('click', toggleForm);

function toggleForm() {
  if (!users.classList.contains('show')) {
    form.classList.toggle('show');
  }
}