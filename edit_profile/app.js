const views = document.getElementById('view');
const form = document.querySelector('.section-one');

views.addEventListener('click', toggleForm);

function toggleForm() {
  if (!views.classList.contains('show')) {
    form.classList.toggle('show');
  }
}
